#include <math.h>
#include <emscripten.h>
#include <stdio.h>
#include <stdlib.h>

#define IX(i,j,k) ((i)+(M+2)*(j) + (M+2)*(N+2)*(k)) 
#define MAX(a,b)            (((a) > (b)) ? (a) : (b))

#define SIZE 8 // Best not to raise this very high

float i = 0;
float interval = 0.02;

extern void dens_step ( int M, int N, int O, float * x, float * x0, float * u, float * v, float * w, float diff, float dt );
extern void vel_step (int M, int N, int O, float * u, float * v,  float * w, float * u0, float * v0, float * w0, float visc, float dt );


//fluid field information
static int M = SIZE; // grid x
static int N = SIZE; // grid y
static int O = SIZE; // grid z
static float dt = 0.4f; // time delta
static float diff = 0.0f; // diffuse
static float visc = 0.0f; // viscosity
static float force = 5.0f;  // added on keypress on an axis
static float source = 100.0f; // density
static float source_alpha =  0.05; //for displaying density

static int addforce[3] = {0, 0, 0};
static int addsource = 0;

static float * u, * v, *w, * u_prev, * v_prev, * w_prev;
static float * dens, * dens_prev;

static int dvel = 0;
static int daxis = 0;

int simCount = 0;

enum { 
	PAN = 1,
	ROTATE,				
	ZOOM				
};		

EMSCRIPTEN_KEEPALIVE
static void free_data ( void )
{
	if ( u ) free ( u );
	if ( v ) free ( v );
	if ( w ) free ( w );
	if ( u_prev ) free ( u_prev );
	if ( v_prev ) free ( v_prev );
	if ( w_prev ) free ( w_prev );
	if ( dens ) free ( dens );
	if ( dens_prev ) free ( dens_prev );
}

EMSCRIPTEN_KEEPALIVE
static void clear_data ( void )
{
	int i, size=(M+2)*(N+2)*(O+2);

	for ( i=0 ; i<size ; i++ ) {
		u[i] = v[i] = w[i] = u_prev[i] = v_prev[i] =w_prev[i] = dens[i] = dens_prev[i] = 0.0f;
	}

	addforce[0] = addforce[1] = addforce[2] = 0;
}

EMSCRIPTEN_KEEPALIVE
static int allocate_data ( void )
{
	int size = (M+2)*(N+2)*(O+2);

	u			= (float *) malloc ( size*sizeof(float) );
	v			= (float *) malloc ( size*sizeof(float) );
	w			= (float *) malloc ( size*sizeof(float) );
	u_prev		= (float *) malloc ( size*sizeof(float) );
	v_prev		= (float *) malloc ( size*sizeof(float) );
	w_prev		= (float *) malloc ( size*sizeof(float) );
	dens		= (float *) malloc ( size*sizeof(float) );	
	dens_prev	= (float *) malloc ( size*sizeof(float) );

	if ( !u || !v || !w || !u_prev || !v_prev || !w_prev || !dens || !dens_prev ) {
		fprintf ( stderr, "cannot allocate data\n" );
		return ( 0 );
	}

	return ( 1 );
}

EMSCRIPTEN_KEEPALIVE
static void get_force_source ( float * d, float * u, float * v, float * w )
{
	int i, j, k, size=(M+2)*(N+2)*(O+2);;

	for ( i=0 ; i<size ; i++ ) {
		u[i] = v[i] = w[i]= d[i] = 0.0f;
	}

	if(addforce[0]==1) // x
	{
		i=2,
		j=N/2;
		k=O/2;

		if ( i<1 || i>M || j<1 || j>N || k <1 || k>O) return;
		u[IX(i,j,k)] = force*10;
		//addforce[0] = 0;
	}	

	if(addforce[1]==1)
	{
		i=M/2,
		j=2;
		k=O/2;

		if ( i<1 || i>M || j<1 || j>N || k <1 || k>O) return;
		v[IX(i,j,k)] = force*10;
		//addforce[1] = 0;
	}	

	if(addforce[2]==1) // y
	{
		i=M/2,
		j=N/2;
		k=2;

		if ( i<1 || i>M || j<1 || j>N || k <1 || k>O) return;
		w[IX(i,j,k)] = force*10; 	
		//addforce[2] = 0;
	}	

	if(addsource==1)
	{
		i=M/2,
		j=N/2;
		k=O/2;
		d[IX(i,j,k)] = source;
		//addsource = 0;
	}
	
	return;
}

float clamp(float x) {
	return x > 360.0f ? x-360.0f : x < -360.0f ? x+=360.0f : x;
}


EMSCRIPTEN_KEEPALIVE
void sim_main(void)
{

    
	get_force_source( dens_prev, u_prev, v_prev, w_prev );
	vel_step ( M,N,O, u, v, w, u_prev, v_prev,w_prev, visc, dt );
	dens_step ( M,N,O, dens, dens_prev, u, v, w, diff, dt );	

}

EMSCRIPTEN_KEEPALIVE
void sim_reset()
{
	clear_data();
}

EMSCRIPTEN_KEEPALIVE
static void key_func ( unsigned char key)
{
	
		switch (key) {
			case 27:		// ESC key
			    free_data ();
			    exit ( 0 );
				break;
			case 'w':       // 'W' key - apply force x-axis
				addforce[1] = 1;
				break;
			case 'd':       // 'D' key - apply force y-axis
				addforce[0] = 1;
				break;
			case 's':       // 'S' key - apply force z-axis
				addforce[2] = 1;
				break;
			case 'x':       // 'X' key - add source at centre
				addsource = 1;
				break;
			case 'c':       // 'C' key - clear simulation
				sim_reset();
				break;
		}
}

EMSCRIPTEN_KEEPALIVE
static void apply_force(int axis, int value)
{
    addforce[axis] = value;
}

EMSCRIPTEN_KEEPALIVE
static void add_fluid(int value) {
    addsource = value;
}


EMSCRIPTEN_KEEPALIVE
static void set_density (unsigned char* lights )
{
    int i, j, k;
	
	float x, y,z, h, d000, d010, d100, d110,d001, d011, d101, d111;
	
	int idx = 0;
	
	for ( i=0; i<=M; i++ ) {

		for ( j=0; j<=N; j++ ) {
		
			for ( k=0; k<=O; k++ ) {


				d000 = dens[IX(i,j,k)];
				d010 = dens[IX(i,j+1,k)];
				d100 = dens[IX(i+1,j,k)];
				d110 = dens[IX(i+1,j+1,k)];

				d001 = dens[IX(i,j,k+1)];
				d011 = dens[IX(i,j+1,k+1)];
				d101 = dens[IX(i+1,j,k+1)];
				d111 = dens[IX(i+1,j+1,k+1)];	
                	
                float aveDensity = (d000+ d010+ d100+ d110+ d001+ d011+ d101+ d111)/8;

				lights[idx] = aveDensity * 255;
				idx++;
			}
		}
	}
}

int main ( int argc, char ** argv )
{

    allocate_data ();
	clear_data ();
	#ifdef __EMSCRIPTEN__
    emscripten_set_main_loop(sim_main, 60, 1);
    #endif
	free_data();

	return 0;
}
