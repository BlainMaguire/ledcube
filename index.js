'use strict';

const WebSocket = require('ws');

var fluid_sim = require('./fluid_sim.js');

var addforce = [];

addforce[0] = addforce[1] = addforce[2] = 0;

var addsource = 0;

const wss = new WebSocket.Server({ port: 8080 });

wss.broadcast = function broadcast(data) {
  wss.clients.forEach(function each(client) {
    if (client.readyState === WebSocket.OPEN) {
      client.send(data);
    }
  });
};

wss.on('connection', function connection(ws) {
  ws.on('message', function incoming(message) {
    console.log('received: %s', message);
    
    		switch (message[0]) {
        		case 'w':       // 'W' key - apply force x-axis
				    addforce[1] = 1 - addforce[1];
				    fluid_sim._apply_force(1, addforce[1]);
				    break;
			    case 'd':       // 'D' key - apply force y-axis
				    addforce[0] = 1 - addforce[0];
				    fluid_sim._apply_force(0, addforce[0]);
				    break;
			    case 's':       // 'S' key - apply force z-axis
				    addforce[2] = 1 - addforce[2];
				    fluid_sim._apply_force(2, addforce[2]);
				    break;
			    case 'x':       // 'X' key - add source at centre
				    addsource = 1 - addsource;
				    fluid_sim._add_fluid(addsource);
			}
			
			
  });

})

//fluid_sim._allocate_data();
//fluid_sim._sim_reset();

let ledCount = 8*8*8;

const directions = ['w', 'd', 's', 'a']; // 'a' is not used in the sim so it opts to do nothing in that case'

let setDensity = fluid_sim.cwrap('set_density',
    null, // Function return void
    ['number'] // array pointer considered number in JS
  );

// Allocate array memory on Emscripten heap and return a pointer
let pArr = fluid_sim._malloc(ledCount);

var lights = new Uint8Array(fluid_sim.HEAP8.buffer, pArr, ledCount);

var i = 0;

setInterval(function() {
            setDensity(pArr);

            /*var axis = 1;
            
            
            
            if ((i % 100) < 50) {
              axis = 0;
            }
            
            if (i < 120) {
             //fluid_sim._add_source(0);

             fluid_sim._apply_force(axis, 1);
            //}
            //else{
              // fluid_sim._apply_force(1, 0);
            //}

            //if (i % 200 < 100 && i < 5000) {
               fluid_sim._add_fluid(1);
            //}
            //else {
             //fluid_sim._add_source(0);
           //}

            
            
            } else {
                fluid_sim._add_fluid(0);
                fluid_sim._apply_force(0, 0);
                fluid_sim._apply_force(1, 0);
            }
            
            i++;
            */
            
            wss.broadcast(lights);
          }, 30);

//fluid_sim._free_data();
